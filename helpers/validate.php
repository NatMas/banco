<?php

function validateespai(){
    $empty = (!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['pass']) && !empty($_POST['dni']) && !empty($_POST['numero']) && !empty($_POST['email']));
    if (!$empty) {
        echo 'Cal introduir totes les dades.';
        return false;
    }else{
        return true;
    }
}
function validateedad(){
    $fecha_nacimiento = $_POST['edad'];
    $nacio = DateTime::createFromFormat('Y-m-d', $fecha_nacimiento);
    $calculo = $nacio->diff(new DateTime());
    $edad = $calculo->y;

    if ($edad < 18) {
        echo 'Menor de 18';
        return false;
    }else{
        return true;
    }
}
function validatenom(){
    $nom = $_POST['nombre'];

    if (!ctype_alpha(str_replace('  ', ' ', $nom))) {
        echo 'Nomès poden apareixer lletres i espais';
        return false;
    }else{
        return true;
    }
}
function validatecognom(){
    $cognom = $_POST['apellido'];

    if (!ctype_alpha(str_replace('  ', ' ', $cognom))) {
        echo 'Nomès poden apareixer lletres i espais';
        return false;
    }else{
        return true;
    }
}

function validatetel1(){
    $telefon = $_POST['numero'];
    if (strlen($telefon) > 9) {
        echo 'Longitud erronea';
        return false;
    }
    if ($telefon[0] == 6 || !$telefon[0] == 7) {
        return true;
    } else {
        echo 'No comença per 6 o 7';
        return false;
    }
}

function validateDNI(){
    $dni = $_POST['dni'];
    $partes = explode('-', $dni);
    $numeros = $partes[0];
    $letra = strtoupper($partes[1]);
    if (!substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros % 23, 1) == $letra) {
        echo 'Lletra incorrecta';
        return false;
    }else{
        return true;
    }
}
function validatePassword(){
    $password = $_POST['pass'];
    //Aa123@567
    if (strlen($password) <= 8 || !preg_match('`[a-z]`', $password) || !preg_match('`[A-Z]`', $password) || !preg_match('`[0-9]`', $password)|| !preg_match('/[\'\/~`\!@#\$%\^&*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',$password)) {
        echo 'Ha de contindre majuscules, minuscules, numeros, un caracter especial i mes de 8 caracters';
        return false;
    }else{
        return true;
    }
}

?>