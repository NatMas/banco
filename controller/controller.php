<link rel="stylesheet" href="../css/style.css"/>
<?php
require_once('../model/cliente.php');
require('../model/ClienteModel.php');

require_once('../model/Cuenta.php');
require('../model/CuentaModel.php');
require('../model/MovimientoModel.php');

use cliente;
session_start();
require_once('../helpers/validate.php');

if (isset($_POST['submit']))
    if (($_POST['switch_lang'] == 'switch_lang')) {
        $lang = $_POST['lang'];
        setcookie('lang', $lang, time() + 60 * 60 * 24 * 30, '/', 'localhost');
        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

if ($_POST['control'] == 'register') {
    if (validateespai() && validatenom() && validatecognom() && validateedad() && validateDNI() && validatetel1() && validatepassword()) {
        $cliente = new Cliente($_POST['nombre'], $_POST['edad'], $_POST['apellido'], $_POST['sexe'], $_POST['email'], $_POST['numero'], $_POST['dni'], $_POST['pass'], "");
        //$cliente = new Cliente("victor","08/08/1999","marquina","h","aaa@gmail.com","6666","2332","123456");
        insertCliente($cliente);
        //Aa123@567 73547999-F
        header('Location: ../views/login.php');
    } else {
        require_once('../views/register.php');
    }
}

if ($_POST['control'] == 'login') {
    $hash = getUserHash($_POST['dni']);
    if (password_verify($_POST['pass'], $hash)) {
        session_start();
        $_SESSION['user'] = $_POST['dni'];
        header('Location: ../views/profile.php');
    } else {
        echo "User o clau incorrecte";
        require_once('../views/login.php');
    }
}
if ($_POST['control'] == 'profile') {
    $check = getimagesize($_FILES['upload']['tmp_name']);
    $filename = $_FILES['upload']['name'];
    $filesize = $_FILES['upload']['size'];
    $filetype = $_FILES['upload']['type'];
    

    if ($check !== false) {
        $image = file_get_contents($_FILES['upload']['tmp_name']);
        updateImg($image);
        header('Location: ../views/profile.php');
    }else{
        $_POST['valid'] ="No hi ha foto";
        require_once('../views/profile.php');
    }

    /*mostrar imagen :
    $obj = selectCliente();
    ob_start();
    fpassthru($obj->getImagen());
    $data = ob_get_contents();
    ob_end_clean();
    $img="data:image/*;base64,".base64_encode($data);
    echo "<img src='".$img."'/>";*/
}
if ($_POST['control'] == 'telefon') {
    if (!empty($_POST['numero'])) {
        if (validatetel1()) {
            $tel = $_POST['numero'];
            updateTel($tel);
            header('Location: ../views/profile.php');
        }
    }else{
        $_POST['valid'] = "No hi ha telefon";
        require_once ('../views/modificartel.php');
    }
}
if ($_POST['control'] == 'email') {
    if (!empty($_POST['email'])) {
        $email = $_POST['email'];
        updateemail($email);
        header('Location: ../views/profile.php');
    }else{
        $_POST['valid'] = "No hi ha correu";
        require_once ('../views/modificarmail.php');
    }
}
if ($_POST['control'] == 'password') {
    if (!empty($_POST['pass'])) {
        if (validatePassword()) {
            $pass = $_POST['pass'];
            updatepass($pass);
            header('Location: ../views/profile.php');
        }
    }else{
        $_POST['valid'] = "No hi ha clau";
        require_once ('../views/modificarpassword.php');
    }
}

if($_POST['control']=='create'){
    session_start();
    createAccount($_SESSION['user']);
    header('Location: ../views/init.php');
}

if ($_POST['control'] == 'select_account') {
    $saldo = getSaldo($_POST['cuentas']);
    session_start();
    $_SESSION['saldo'] = $saldo;
    $_SESSION['lista'] = getMovimientos($_POST['cuentas']);
    header("Location: ../views/query.php");
}


if($_POST['control']=='transfer') {
    if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
        transfer($_POST['cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
        header("Location: ../views/query.php");
    }
}
if($_POST['control']=='query') {
    session_start();
    $_SESSION['saldo']=getSaldo($_POST['cuentas']);
    $_SESSION['lista']=getMovimientos($_POST['cuentas']);
    header("Location: ../views/query.php");
}

?>
