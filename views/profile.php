<?php
require_once('../model/cliente.php');
require('../model/ClienteModel.php');

use cliente;
session_start();
$obj = selectCliente();
ob_start();
fpassthru($obj->getImagen());
$data = ob_get_contents();
ob_end_clean();
$img="data:image/*;base64,".base64_encode($data);
//echo "<img src='".$img."' style= 'width : 200px'/>";
?>

<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../css/style.css" />

    <title>
        profile
    </title>
</head>

<body>
<a href="../views/init.php">init</a>
<a href="../views/transfer.php">Transferencias</a>
<a href="../views/register.php">Register</a>
<a href="../views/logout.php">Logout</a>

<form action="../controller/controller.php" method="post" enctype="multipart/form-data">
    <h3>Dades Actuals:</h3>
    <label>
        <?php echo "<img src='".$img."' style= 'width : 200px'/>";?>
        <br/>
    </label>
    <label>
        Nom:

    <?php
    $nombre = $obj->getNombre();
    echo "$nombre"."</br>";

    ?>
    </label>
    <label>
    Cognom:

    <?php
    $apellidos = $obj->getApellidos();
    echo "$apellidos"."</br>";

    ?>
    </label>
    <label>
    Naixement:

    <?php
    $nacimiento = $obj->getNacimiento();
    echo "$nacimiento"."</br>";

    ?>
    </label>
    <label>
    Sexe:

    <?php
    $genero = $obj->getSexo();
    echo "$genero"."</br>";

    ?>
    </label>
    <label>
        Email:

        <?php
        $mail = $obj->getEmail();
        echo "$mail"."</br>";

        ?>
    </label>
    <label>
        DNI:

        <?php
        $dni = $obj->getDni();
        echo "$dni"."</br>";

        ?>
    </label>
    <label>
        Telefon:

        <?php
        $tel = $obj->getTelefono();
        echo "$tel"."</br>";

        ?>
    </label>

    <h3>Dades a modificar:</h3>

    Select img to upload:
    <input type="file" name="upload" id="upload">

    <p>
        <a href="modificartel.php">Modificar Telefon</a>
    </p>
    <p>
        <a href="modificarmail.php">Modificar mail</a>
    </p>
    <p>
        <a href="modificarpassword.php">Modificar password</a>
    </p>

    <input name="control" value="profile" type="hidden"/>
    <input name="submit" value="submit" type="submit"/>

</form>
<?php

if (isset($_POST['valid'])){
    echo $_POST['valid'];
}
?>
</body>
</html>
