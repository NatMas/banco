<?php
require_once('../helpers/i18n.php');
?>

<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="../css/style.css"/>
    <title>
        <?php echo _("Registro")?>
    </title>
</head>

<body>
<?php require_once('header.php');?>
<a href="../views/login.php">Login</a>
<form action="../controller/controller.php" method="post">

    <p>
        <label>
            Nom:
            <input name="nombre" type="text"/>
        </label>
    </p>
    <p>
        <label>
            Cognom:
            <input name="apellido" type="text"/>
        </label>
    </p>
    <select name="sexe">
        <option>A</option>
        <option>M</option>
        <option>F</option>
    </select>
    <p>
        <label>
            Fecha de nacimiento
            <input name="edad" type="date"/>
        </label>

    </p>

    <p>
        <label>
            DNI ( - ):
            <input name="dni" type="text"/>
        </label>
    </p>
    <p>
        <label>
            Numero:
            <input name="numero" type="tel"/>
        </label>
    </p>
    <p>
        <label>
            Mail:
            <input name="email" type="email"/>
        </label>
    </p>
    <p>
        <label>
            Pass:
            <input name="pass" type="password"/>
        </label>
    </p>

    <input name="control" value="register" type="hidden"/>
    <input name="submit" value="submit" type="submit"/>
</form>


<?php
if (isset($_POST['message'])){
    $_POST['message'];
}
?>
</body>
</html>
