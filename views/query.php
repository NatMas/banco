<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../css/style.css" />
    <title>
        query
    </title>
</head>

<body>
<?php
session_start();
if (isset($_SESSION['user'])){ ?>
<a href="init.php">init</a>
<a href="transfer.php" >Transferencias</a>
<a href="profile.php">perfil</a>
<a href="logout.php">Logout</a>

<form action="../controller/controller.php" method="post">
    <select name="cuentas">

            <?php
            require_once('../model/CuentaModel.php');
            $accounts=getAccounts('dni');
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="query"/>
</form>

<?php

if (isset($_SESSION['saldo'])) {
    echo "Saldo " . $_SESSION['saldo'] . '<br/>';
}
if (isset($_SESSION['lista'])) {
    $movimientos=$_SESSION['lista'];
    echo '<table class="default" rules="all" frame="border">';
    echo '<tr>';
    echo '<th>origen</th>';
    echo '<th>destino</th>';
    echo '<th>hora</th>';
    echo '<th>cantidad</th>';
    echo '</tr>';
    for ($i=0;$i<count($movimientos);$i++){
        echo '<tr>';
        echo '<td>'.$movimientos[$i]['id_origen'].'</td>';
        echo '<td>'.$movimientos[$i]['id_destino'].'</td>';
        echo '<td>'.$movimientos[$i]['fecha'].'</td>';
        echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
        echo '</tr>';
    }
    echo '</table>';
}
}else{
    header('Location: ../views/login.php');

}?>

</body>
</html>
