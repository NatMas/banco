<?php
require_once('../helpers/DBManager.php');
session_start();
function transfer($origen, $destino, $cantidad)
{
    $manager = new DBManager();
    try {
        $sql = "INSERT INTO movimientos (fecha,cantidad,id_origen,id_destino) VALUES (now(),:cantidad,:origen,:destino)";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':origen', $origen);
        $stmt->bindParam(':destino', $destino);
        $stmt->bindParam(':cantidad', $cantidad);
        $stmt->execute();

        $sql = "UPDATE cuenta SET saldo = saldo - $cantidad WHERE cuenta=:origen";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':origen', $origen);
        $stmt->execute();

        $sql = "UPDATE cuenta SET saldo = saldo + $cantidad WHERE cuenta=:destino";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':destino', $destino);
        $stmt->execute();

        $manager->cerrarConexion();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}