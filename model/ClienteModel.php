<?php
require_once('../helpers/DBManager.php');
use DBManager;
session_start();
function selectCliente(){
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt -> bindParam(':dni',$_SESSION['user']);
        $stmt ->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $obj = new cliente($result[0]['nombre'],$result[0]['nacimiento'],$result[0]['apellidos'],$result[0]['sexo'],$result[0]['email'],$result[0]['telefono'],$result[0]['dni'],$result[0]['password'],$result[0]['imagen']);
        return $obj;
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateImg($image){
    $manager = new DBManager();
    try {
        $sql = "UPDATE cliente SET imagen=:img where dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt -> bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt -> bindParam(':dni',$_SESSION['user']);
        $stmt ->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}
function updateTel($tel){
    $manager = new DBManager();
    try {
        $sql = "UPDATE cliente SET telefono=:telefono where dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt -> bindParam(':telefono',$tel,PDO::PARAM_LOB);
        $stmt -> bindParam(':dni',$_SESSION['user']);
        $stmt ->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}
function updateemail($email){
    $manager = new DBManager();
    try {
        $sql = "UPDATE cliente SET email=:email where dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt -> bindParam(':email',$email,PDO::PARAM_LOB);
        $stmt -> bindParam(':dni',$_SESSION['user']);
        $stmt ->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}
function updatepass($pass){
    $manager = new DBManager();
    try {
        $sql = "UPDATE cliente SET password=:password where dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $password=password_hash($pass,PASSWORD_DEFAULT,['cost'=>10]);
        $stmt -> bindParam(':password',$password,PDO::PARAM_LOB);
        $stmt -> bindParam(':dni',$_SESSION['user']);
        $stmt ->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}


function insertCliente($cliente){

    $manager = new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre, nacimiento, apellidos, sexo, email, telefono,dni , password, imagen) VALUES (:nombre,:nacimiento,:apellidos,:sexo,:email,:telefono,:dni,:password,:imagen)";

        $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=>10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$cliente->getNombre());
        $stmt->bindParam(':apellidos',$cliente->getApellidos());
        $stmt->bindParam(':nacimiento',$cliente->getNacimiento());
        $stmt->bindParam(':sexo',$cliente->getSexo());
        $stmt->bindParam(':telefono',$cliente->getTelefono());
        $stmt->bindParam(':dni',$cliente->getDni());
        $stmt->bindParam(':email',$cliente->getEmail());
        $stmt->bindParam(':imagen',$cliente->getImagen());
        $stmt->bindParam(':password',$password);

        if ($stmt->execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }

    }catch (PDOException $e){
        echo $e->getMessage();
    }

}
function getUserHash($dni){
    $conexion = new DBManager();

    try{
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result  = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo $result;
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}


?>